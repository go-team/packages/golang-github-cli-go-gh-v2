golang-github-cli-go-gh-v2 (2.6.0-2) unstable; urgency=medium

  * Team upload.
  * Add missing build dependency on tzdata

 -- Jochen Sprickerhof <jspricke@debian.org>  Sun, 02 Mar 2025 07:44:06 +0100

golang-github-cli-go-gh-v2 (2.6.0-1) unstable; urgency=medium

  * New upstream version 2.6.0

 -- Anthony Fok <foka@debian.org>  Tue, 05 Mar 2024 09:36:01 -0700

golang-github-cli-go-gh-v2 (2.5.0-1) unstable; urgency=medium

  * Set debian/watch to track tagged releases again
  * New upstream version 2.5.0

 -- Anthony Fok <foka@debian.org>  Wed, 07 Feb 2024 05:02:38 -0700

golang-github-cli-go-gh-v2 (2.4.0+git20231120.d32c104-1) unstable; urgency=medium

  * Set debian/watch to track git repo HEAD with pretty=2.4.0+git%cd.%h
  * New upstream version 2.4.0+git20231120.d32c104
  * Bump versioned dependencies as per go.mod

 -- Anthony Fok <foka@debian.org>  Wed, 27 Dec 2023 06:51:03 -0700

golang-github-cli-go-gh-v2 (2.3.0-1) unstable; urgency=medium

  * New upstream version 2.3.0
  * Update versioned dependencies as per go.mod

 -- Anthony Fok <foka@debian.org>  Mon, 04 Dec 2023 04:10:24 -0700

golang-github-cli-go-gh-v2 (2.0.0-2) unstable; urgency=medium

  * Source-only upload for migration to testing

 -- Anthony Fok <foka@debian.org>  Sun, 15 Oct 2023 17:00:16 -0600

golang-github-cli-go-gh-v2 (2.0.0-1) unstable; urgency=medium

  * Prepare for new v2 package: golang-github-cli-go-gh-v2
  * New upstream version 2.0.0
  * Bump versioned dependencies as per go.mod

 -- Anthony Fok <foka@debian.org>  Sun, 15 Oct 2023 03:03:19 -0600

golang-github-cli-go-gh (1.2.1-1) unstable; urgency=medium

  * New upstream version 1.2.1
  * Bump versioned dependencies as per go.mod

 -- Anthony Fok <foka@debian.org>  Mon, 03 Jul 2023 11:36:33 -0600

golang-github-cli-go-gh (1.0.0-1) unstable; urgency=medium

  * New upstream version 1.0.0
  * Bump Standards-Version to 4.6.2 (no change)

 -- Anthony Fok <foka@debian.org>  Wed, 25 Jan 2023 15:15:44 -0700

golang-github-cli-go-gh (0.1.2-1) unstable; urgency=medium

  * New upstream version 0.1.2
  * Set debian/watch to track tagged releases again
  * Build-depend explicitly on golang-any (>= 2:1.18~) as per go.mod
  * Bump versioned dependencies as per go.mod

 -- Anthony Fok <foka@debian.org>  Fri, 21 Oct 2022 01:10:14 -0600

golang-github-cli-go-gh (0.1.0+git20220919.802b578-1) unstable; urgency=medium

  * New upstream version 0.1.0+git20220919.802b578

 -- Anthony Fok <foka@debian.org>  Wed, 28 Sep 2022 07:00:47 -0600

golang-github-cli-go-gh (0.1.0+git20220825.34c1b91-1) unstable; urgency=medium

  * New upstream version 0.1.0+git20220825.34c1b91
  * Set debian/watch to track git repo HEAD with pretty=0.1.0+git%cd.%h
  * Update dependencies as per go.mod

 -- Anthony Fok <foka@debian.org>  Mon, 05 Sep 2022 01:42:45 -0600

golang-github-cli-go-gh (0.1.0-1) unstable; urgency=medium

  * New upstream version 0.1.0
  * Set debian/watch to track tagged releases again
  * Add new dependencies as per go.mod

 -- Anthony Fok <foka@debian.org>  Thu, 11 Aug 2022 18:59:20 -0600

golang-github-cli-go-gh (0.0.3+git20220623.91ca4ef-2) unstable; urgency=medium

  * Depend on golang-gopkg-h2non-gock.v1-dev (>= 1.1.2) version explicitly.
    Thanks to Paul Gevers for the autopkgtest regression report!
    (Closes: #1016740)
  * Remove 0001-skip-tests-that-access-the-internet.patch.
    Those tests previously failed not because of lack of Internet access
    but because of incorrect version of gopkg.in/h2non/gock.v1 dependency.

 -- Anthony Fok <foka@debian.org>  Wed, 10 Aug 2022 16:16:30 -0600

golang-github-cli-go-gh (0.0.3+git20220623.91ca4ef-1) unstable; urgency=medium

  * New upstream version 0.0.3+git20220623.91ca4ef
  * Skip tests that access the Internet during Debian build
    but let them run during autopkgtest.

 -- Anthony Fok <foka@debian.org>  Mon, 18 Jul 2022 20:36:04 -0600

golang-github-cli-go-gh (0.0.3+git20220614.ef2bca9-1) unstable; urgency=medium

  * New upstream version 0.0.3+git20220614.ef2bca9
  * Set debian/watch to track git repo HEAD with pretty=0.0.3+git%cd.%h

 -- Anthony Fok <foka@debian.org>  Fri, 17 Jun 2022 06:54:03 -0600

golang-github-cli-go-gh (0.0.3-1) unstable; urgency=medium

  * Initial release (Closes: #1012796)

 -- Anthony Fok <foka@debian.org>  Tue, 14 Jun 2022 03:30:32 -0600
